<?php

namespace App\Http\Controllers;

use http\Client\Response;
use Illuminate\Http\Request;
use function response;
use function sleep;
use function var_dump;

class HomeController extends Controller
{

    public function home()
    {
        $items=[];

        for($i=1;$i<=100;$i++)
        {
            $items[]=['id'=>$i, 'name'=>'Turbine Item '.$i];
        }

        return view('home', ['items'=>$items]);
    }


    public function inspectItem(Request $request){

        ////added for demo so that the messages can be shown
        sleep(1);
        $request->validate([
            'id' => 'integer|required',
        ]);

        $id = $request->input(['id']);

        if ($id % 3 == 0 && $id % 5 == 0)
        {
            $message='Coating Damage and Lightning Strike';
            $messageCode='CDLS';

        }elseif ($id % 3 == 0)
        {
            $message='Coating Damage';
            $messageCode='CD';

        }elseif ($id % 5 == 0)
        {
            $message='Lightning Strike';
            $messageCode='LS';

        }else{
            $message=$id;
            $messageCode='';
        }

        $output=['status'=>'OK','messageCode'=>$messageCode,'message'=>$message];

        return response()->json($output);;
    }

}
