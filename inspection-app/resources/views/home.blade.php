<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Wind Turbine</title>
    <!-- jQuery Library -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js"
            integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <!-- Noty Library -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/noty/3.1.4/noty.min.js"
            integrity="sha512-lOrm9FgT1LKOJRUXF3tp6QaMorJftUjowOWiDcG5GFZ/q7ukof19V0HKx/GWzXCdt9zYju3/KhBNdCLzK8b90Q=="
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/noty/3.1.4/noty.min.css"
          integrity="sha512-0p3K0H3S6Q4bEWZ/WmC94Tgit2ular2/n0ESdfEX8l172YyQj8re1Wu9s/HT9T/T2osUw5Gx/6pAZNk3UKbESw=="
          crossorigin="anonymous"/>


    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <!-- Bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">

    <style>

        body {
            background: url("{{url('images/windmill-image.gif')}}") no-repeat fixed center;
            font-family: 'Nunito', sans-serif;
            background-size: 100% 100%;
        }

        .console-box {
            height: 200px;
            background-color: rgb(27 22 22 / 50%);
            color: white;
        }

        #inspectTurbineBtn {
            float: right;
        }
    </style>
</head>
<body>

<div class="container">
    <h1 class="text-white text-center p-4">Wind Turbines</h1>

    <div class="row">
        <div class="col-sm">
            <img class="light-strike" src="{{url('images/lightening-strike.png')}}" style="display: none; width: 200px;">
        </div>
        <div class="col-sm">

            <div class="border border-3 border-white rounded p-4 overflow-auto console-box">

            </div>

            <button type="button" class="mt-2 btn btn-secondary btn-sm" id="inspectTurbineBtn">Inspect Wind Turbine
                Items
            </button>

        </div>
    </div>
</div>

<script>

    $(document).ready(function () {
        inspectTurbineItems();
    });

    $('#inspectTurbineBtn').click(function () {
        inspectTurbineItems();
    });

    function inspectTurbineItems() {

        $('#inspectTurbineBtn').hide();

        let items =<?= json_encode($items);?>;

        $.each(items, function (index, item) {

            inspectItem(item);
        });

        $('.light-strike').hide();

    }///end of function inspectTurbineItems() {


    function inspectItem(item) {
        let inspect_url = '<?= url('inspect-item');?>';

        $.ajaxQueue({
            url: inspect_url,
            type: 'GET',
            contentType: false,
            processData: true,
            data: {'id': item['id']},

            success: function (response) {
                console.log(response);
                if (response) {
                    let msg = item['name'] + ' - ' + response.message;

                    $('.console-box').append(msg + '<br>');
                    $('.console-box').scrollTop($('.console-box')[0].scrollHeight);

                    $('.light-strike').hide();

                    if (response.messageCode == 'CD') {
                        notify('warning', msg);
                    }

                    if (response.messageCode == 'LS') {
                        notify('alert', msg);
                        $('.light-strike').show();
                    }

                    if (response.messageCode == 'CDLS') {
                        notify('info', msg);
                        $('.light-strike').show();

                    }

                    if (item['id']==100)
                    {
                        $('#inspectTurbineBtn').show();

                    }

                }
            },

            error: function (response) {

                if (response) {
                    let msg = item['name'] + ' - ' + response.message;
                    $('.console-box').append(msg + '<br>');
                    notify('error', msg);
                    $('.console-box').scrollTop($('.console-box')[0].scrollHeight);
                }
                if (item['id']==100)
                {
                    $('#inspectTurbineBtn').show();

                }
            },

        });

    }//end of function inspectItem


    function notify(type, message) {
        new Noty({
            type: type,
            layout: 'bottomRight',
            theme: 'relax',
            dismissQueue: false,
            force: false,
            maxVisible: 2,
            timeout: 1000,
            text: message
        }).show();
    }


    //This handles the queues
    (function ($) {

        var ajaxQueue = $({});

        $.ajaxQueue = function (ajaxOpts) {

            var oldComplete = ajaxOpts.complete;

            ajaxQueue.queue(function (next) {

                ajaxOpts.complete = function () {
                    if (oldComplete) oldComplete.apply(this, arguments);

                    next();
                };

                $.ajax(ajaxOpts);
            });
        };

    })(jQuery);


</script>

</body>
</html>


