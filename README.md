# Wind Turbine Project

### Task

A wind turbine has 100 items on it, each gets inspected. Write an application that outputs numbers from 1 to 100, but for multiples of three print “Coating Damage” instead of the number and for the multiples of five print “Lightning Strike” instead of the number. For numbers which are multiples of both three and five print “Coating Damage and Lightning Strike” instead of the number.
 
The code to generate the output should be done in PHP and the output should be rendered in HTML with a nice user experience. Share the code to us and show us what you can do.
 
#### Extras

* A nice UI that makes the most of the output ✓
* Pulling the inspection data down in an Ajax call and processing the results in the browser ✓
* Putting the code in GitHub for us to see ✓
* Getting it to run on a free AWS instance ✓
* Sharing your plan and approach (any notes you made before coding started) ✓
* Any future plans for how the application could be improve ✓

   
### Demo Link (AWS EC-2 Instance)
http://ec2-18-188-140-172.us-east-2.compute.amazonaws.com/


**PLEASE MAKE SURE YOU RUN THIS WITH HTTP**

**NOT HTTPS**

### Project Description

##### Backend 
- Laravel Framework 8.40.0
##### Frontend
- jQuery 3.6
- Bootstrap 5
- Noty 3.1.4

The project uses 2 urls (both developed using Laravel)
1. The main page URL or the home page URL
2. The inspection URL that get the item id and generate the output in JSON 

The turbine items array is initially passed from the main page. 
Using jQuery and AJAX, each item id is individually sent to the inspection URL. 
The inspection URL get the id, performs operation _(checks weather id is multiple of 3 or 5 or both)_ and returns back the output in JSON.
In addition, along with the message, I have also passed status and message code, that helps system to understand the output from system perspective. 
There has been additional validation added using Laravel validation, to check that id is a required field and its an integer. 
These errors are also handled when response is returned in AJAX call using jQuery

An additional jQuery function has been defined too, so that the ajax calls are handled in a queue. The next ajax calls only starts when the current ajax calls finishes. 
This helps server not to go through several requests at the same time and also keeps the server efficient.  

Also, just for this demo, a sleep timer has been added on inspection URL _(HomeController.php line 29)_ so that the response & notification looks good on UI

The frontend has been designed using some core CSS along with the Bootstrap 5 libraries 
The Noty library helps to display notification on screen with different colors based on severity. 

The Bootstrap, Noty and jQuery and implemented using CDN. 

##### AWS EC2 Instance

The project has been deployed on the AWS EC2 Instance (link provided above) with Instance type _t2.micro_.

The Inbound ports open for communication are 
- PORT 22: For SSH Access
- PORT 80: For HTTP communication
- PORT 443 For HTTPS communication ( Need to obtain a ssl certificate for this)

The Storage on the instance is 8GB

The EC2 instance runs apache 2 with PHP 7.3

http://ec2-18-188-140-172.us-east-2.compute.amazonaws.com/

Also, the Document root in apache config changed from 

_/var/www/html_ 

to 

_/var/www/html/windturbine/inspection-app/public_

so that the application can be run from the laravel public folder and application is secured



#### Project Plan Steps 

1. Setup Gitlab ✓
1. Setup AWS ✓
1. Setup Laravel ✓
1. Setup the Main Page (loading page)  ✓
1. Load the wind Turbines data (in array) ✓
1. Setup Bootstrap & layout  ✓
1. Setup jQuery  ✓
1. Set a button to run loop of items again ✓
1. Send the current item via ajax and get the result (“Coating Damage, Lightning Strike, Coating Damage and Lightning Strike” ) ✓
1. Set the background images from the asset ✓
1. Create a console on page to show output ✓
1. Set the lightening strike image  ✓
1. Show coating damage box using css ✓
1. Testing ✓ 
1. Fine Tuning the Code ✓
1. Update the Readme ✓

### Gitlab Access

Currently, the Gitlab access is set to public 

https://gitlab.com/sudeepatalati/windturbine

### Future Plans / Recommendations
1. A SSL certificate could be installed on the Domain
1. For better UI better images can be implemented
1. Auto deploy pipeline like GIT LAB CI/CD should be implemented when doing continuous delivery, deployment or integration with test procedures
1. Custom JS, CSS and Images can be moved to resources folder and can be deployed using node. SCSS should be used if using a custom CSS for large projects
1. CDN (Content Delivery Network) should be used to deliver custom CSS, JS & Images in case of high traffic website
1. If required, jQuery Bootstrap and other libraries can be installed locally to avoid dependencies from thrid party sources.
